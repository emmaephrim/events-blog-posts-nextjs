const { MongoClient } = require("mongodb");

const url =
  "mongodb+srv://emmaephrim:Q69oDrfngWxZTpIm@cluster0.4is9wz2.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(url);
const dbName = "events";

function handler(req, res) {
  if (req.method == "POST") {
    const eventId = req.query.eventId;
    const { email, name, text } = req.body;

    if (
      !email.includes("@") ||
      !name == name.trim("") ||
      !text === text.trim("")
    ) {
      res.status(422).json({ message: "Invalid inputs!" });
      return;
    }
    const newComment = {
      name,
      email,
      text,
      eventId,
    };

    async function main() {
      // Use connect method to connect to the server
      await client.connect();
      console.log("Connected successfully to server");
      const db = client.db(dbName);
      const collection = db.collection("comments");
      await collection.insertOne(newComment);
      return "done.";
    }
    try {
      main()
        .then(console.log)
        .catch(console.error)
        .finally(() => client.close());
    } catch (error) {
      console.log(error);
    }

    console.log(name, email, text);
    res.status(201).json({ message: "Comment Added", comment: newComment });
  }

  if (req.method == "GET") {
    async function main() {
      // Use connect method to connect to the server
      await client.connect();
      console.log("Connected successfully to server");
      const db = client.db(dbName);
      const collection = db.collection("comments");
      const findResult = await collection.find({}).sort({ _id: -1 }).toArray();
      console.log("Found documents =>", findResult);
      await res.status(200).json({ comments: findResult });
      return "done!";
    }
    main()
      .then(console.log)
      .catch(console.error)
      .finally(() => client.close());
  }
}

export default handler;
