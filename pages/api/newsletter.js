const { MongoClient } = require("mongodb");

const url =
  "mongodb+srv://emmaephrim:Q69oDrfngWxZTpIm@cluster0.4is9wz2.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(url);
const dbName = "events";

function handler(req, res) {
  if (req.method == "POST") {
    const userEmail = req.body.email;

    if (!userEmail || !userEmail.includes("@")) {
      res.status(422).json({ message: "Invalid Email Address" });
      return;
    }
    async function main() {
      // Use connect method to connect to the server
      await client.connect();
      console.log("Connected successfully to server");
      const db = client.db(dbName);
      const collection = db.collection("newsletter");
      await collection.insertOne({ email: userEmail });
      return "done.";
    }
    try {
      main()
        .then(console.log)
        .catch(console.error)
        .finally(() => client.close());
    } catch (error) {
      console.log(error);
    }

    console.log("User Email: ", userEmail);
    res.status(201).json({ message: "Signed Up" });
  }
}

export default handler;
